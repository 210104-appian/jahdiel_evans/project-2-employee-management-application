# Employee Management Application

## Project Description
An Appian application with some functionality to assist in employee management. Includes many additional and unnecessary design objects that were created for the purposes of learning.

## Technologies used
- Appian 20.4
- MySQL Database
- SQL
- JDBC

## Features
- Working Process model, with write to datastore entity and write to multiple datastore entity.
- Employee data, secondary, and address data being commited.
- Expression rules fetching employees.
- Paging and editable grids displaying info and enabling updates.
- Functioning records, sites, tasks, tempo, and reports.

## Getting Started
- Requires being created as a user for the Revature Appian Cloud development environment.
- Log into the Appian Cloud environment using credentials given by a system administrator: [Login Page](https://revaturedev.appiancloud.com/).
- To work in Tempo, select Actions and click on JEEM Add New Employee to add a new employee.
- Click on records and select JEEM Records to view a grid list of all the employees in the system.
- Can filter employees via search, salary, and the status.
- View the Employee Drill Down report by clicking on Reports and selecting JEEM Employee Report
- Can view Approved, Pending and Rejected employees by clicking on the respective area of the pie chart
- Can get a similar experience using the site by clicking on the navigation bar and selecting JEEM HR Site

## Usage
This application was created for learning purposes only. 
Large portions of this application were used as a sand box for testing different functionality
in Appian and learning how to use different features.

## License
This project uses the following license: [MIT License](https://gitlab.com/210104-appian/jahdiel_evans/project-2-employee-management-application/-/blob/master/LICENSE).
